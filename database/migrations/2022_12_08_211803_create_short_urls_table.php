<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShortUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('short_urls', function (Blueprint $table) {
            $table->id();
            $table->string('code',8)->nullable()->comment('短网址code');
            $table->string('long_url',1024)->nullable()->comment('长网址');
            $table->string('long_url_hash',32)->nullable()->comment('长网址做hash后的值');
            $table->integer('request_num')->default(0)->comment('请求次数');
            $table->string('ip',32)->nullable()->comment('请求ip');
            $table->string('access_key','32')->nullable()->comment('密钥');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('short_urls');
    }
}
