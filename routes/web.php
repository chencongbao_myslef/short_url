<?php
use Illuminate\Support\Facades\Route;
use EasyShortUrl\Http\Controller\ShortUrlController;

Route::get('/s/{code}', ShortUrlController::class.'@redirectLongUrl');
