<?php
return [
    /*
     * 是否开启缓存，可选项 0: 不开启, 1: 开启 (开启缓存，数据表跳转统计将失效)
     */
    "esu_domain" => env('ESU_DOMAIN', env('APP_URL', '')),

    /**
     * 是否开启缓存，可选项 0: 不开启, 1: 开启 (开启缓存，数据表跳转统计将失效)
     */
    "esu_cache_open" => env("ESU_CACHE_OPEN", 0),


    /***
     * # 缓存方式，可选项 file: 本地文件缓存, redis: 缓存 (Redis 缓存，依赖 ESU_REDIS_DSN 配置)
     */
    'esu_cache_client' => env("esu_cache_client", 'file'),

    /**
     * 默认缓存时间 604800 秒 (1星期)
     */
    "esu_cache_lifetime" => env('ESU_CACHE_LIFETIME', '604800'),

    /**
     * web_admin 页 access_key
     */
    "esu_web_admin_access_key" => env('ESU_WEB_ADMIN_ACCESS_KEY', 'esu')


];
