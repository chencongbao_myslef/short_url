<?php

namespace EasyShortUrl;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ShortUrlServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->registerPublishing();

        $this->mergeConfigFrom(dirname(__DIR__).'/config/shorturl.php', 'shorturl');

        Route::middleware('web')->group(dirname(__DIR__).'/routes/web.php');

    }


    protected function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../config' => config_path()], 'short-url-config');
            $this->publishes([__DIR__.'/../database/migrations' => database_path('migrations')], 'short-url-migrations');
        }
    }
}
