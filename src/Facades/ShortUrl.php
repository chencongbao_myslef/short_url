<?php
namespace EasyShortUrl\Facades;

use EasyShortUrl\EasyShortUrl;

class ShortUrl extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return EasyShortUrl::class;
    }
}
